package com.edwise.completespring.assemblers;

import com.edwise.completespring.entities.Foo;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by user EAnton on 25/04/2014.
 */
public class FooResource extends ResourceSupport {
    public Foo foo;
}

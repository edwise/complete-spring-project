package com.edwise.completespring.assemblers;

import com.edwise.completespring.entities.Book;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by user EAnton on 04/04/2014.
 */
public class BookResource extends ResourceSupport {
    public Book book;
}

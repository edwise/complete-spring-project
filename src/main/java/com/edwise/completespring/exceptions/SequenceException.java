package com.edwise.completespring.exceptions;

/**
 * Created by user EAnton on 07/04/2014.
 */
public class SequenceException extends RuntimeException {

    public SequenceException(String msg) {
        super(msg);
    }
}

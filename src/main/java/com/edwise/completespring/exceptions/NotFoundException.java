package com.edwise.completespring.exceptions;

/**
 * Created by user EAnton on 07/04/2014.
 */
public class NotFoundException extends RuntimeException {

    public NotFoundException(String msg) {
        super(msg);
    }
}

**IMPORTANTE: Este proyecto ha sido migrado a github en la siguiente url : https://github.com/edwise/complete-spring-project . Ya no voy a actualizarlo aquí en bitbucket, si te interesa seguir los nuevos cambios, accede a mi repo en github.**


Proyecto montado con Spring Boot, con los siguientes frameworks / libraries / funcionalidades:

 - Servicio completo RESTful con Spring 4 (Books)

 - Uso de HATEOAS en el servicio

 - Documentado servicio con Swagger

 - Capa de base de datos con Spring DATA mongoDB

 - Jacoco para la cobertura de tests (plugin para maven)

 - Lombok para evitar algo de código 'boilerplate'

 - Spring Exception Handling en los controllers



 Requisitos:

 - Maven (instalado y configurado)

 - mongoDB server (instalado y arrancado, en localhost y con el puerto por defecto)



Urls de acceso:

 - Swagger check -> http://localhost:8080/api-docs

 - Swagger UI    -> http://localhost:8080/index.html

 - REST Books    -> http://localhost:8080/api/book

 - Jacoco        -> DIRECTORIO_PROYECTO/target/sites/jacoco/index.html



Fuentes:

 - http://www.spring.io/

 - http://www.mkyong.com/mongodb/spring-data-mongodb-auto-sequence-id-example/